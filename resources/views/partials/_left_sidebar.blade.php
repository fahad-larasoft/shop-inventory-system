<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                            <img alt="image" style="height: 50px; width: 50px;" class="img-circle" src="/images/default.png" />
                             </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs">
                                    <strong class="font-bold">
                                        {!! auth()->user()->name !!}
                                    </strong>
                             </span> <span class="text-muted text-xs block">Admin <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        {{--<li><a href="profile.html">Profile</a></li>--}}
                        {{--<li><a href="contacts.html">Contacts</a></li>--}}
                        {{--<li><a href="mailbox.html">Mailbox</a></li>--}}
                        {{--<li class="divider"></li>--}}
                        <li><a href="/logout">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li class="{!! request()->is('/') ? 'active' : '' !!}">
                <a href="/"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span> </a>
                {{--<ul class="nav nav-second-level">--}}
                    {{--<li class="active"><a href="index.html">Dashboard v.1</a></li>--}}
                    {{--<li><a href="dashboard_2.html">Dashboard v.2</a></li>--}}
                    {{--<li><a href="dashboard_3.html">Dashboard v.3</a></li>--}}
                    {{--<li><a href="dashboard_4_1.html">Dashboard v.4</a></li>--}}
                    {{--<li><a href="dashboard_5.html">Dashboard v.5 <span class="label label-primary pull-right">NEW</span></a></li>--}}
                {{--</ul>--}}
            </li>
            <li class="{!! request()->is('godown*') ? 'active' : '' !!}">
                <a href="/godown"><i class="fa fa-sitemap"></i> <span class="nav-label">Manage Godowns</span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="/godown">  Manage Godown</a></li>
                    <li><a href="/godown/create"> <i class="fa fa-plus" aria-hidden="true"></i>Add Godown</a></li>


                </ul>
            </li>

            <li class="{!! request()->is('items*') ? 'active' : '' !!}">
                <a href="#"><i class="fa fa-desktop"></i> <span class="nav-label">Inventory Purchases</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="/items"><i class="fa fa-sitemap"></i> Manage Items</a></li>
                    <li><a href="/items/create"> <i class="fa fa-plus" aria-hidden="true"></i> Add Item</a></li>
                </ul>
            </li>
            <li class="{!! request()->is('sales*') ? 'active' : '' !!}">
                <a href="#"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Inventory Sales</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="/sales"><i class="fa fa-sitemap"></i> Manage Sales</a></li>
                    <li><a href="/sales/create"><i class="fa fa-plus" aria-hidden="true"></i> Add Sale</a></li>
                </ul>
            </li>
            <li class="{!! request()->is('settings*') ? 'active' : '' !!}">
                <a href="{!! route('settings.index') !!}"><i class="fa fa-gear"></i> <span class="nav-label">Settings</span></a>
            </li>
        </ul>

    </div>
</nav>
