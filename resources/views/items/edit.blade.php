@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Item</div>
                    @if(Session::has('message'))

                        <div class="alert alert-info">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Info!</strong>  {{Session::get('message')}}
                        </div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="panel-body">
                        {!!Form::model($item, array('route' => ['items.update' , $item->id]  , 'method' => 'put', 'files' => 'true'))!!}

                        @include('items._form')

                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection