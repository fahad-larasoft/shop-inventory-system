
<div class="form-group{{ $errors->has('godown_id') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Godowns</label>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::select('godown_id', $godowns, null, ['class' => 'form-control', 'id' => 'godown_id']) !!}
        </div>

        @if ($errors->has('godown_id'))
            <span class="help-block">
                                        <strong>{{ $errors->first('godown_id') }}</strong>
                                    </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('serial_number') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Sr #</label>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::text('serial_number', null, ['class' => 'form-control', 'id' => 'serial_number']) !!}
        </div>
        @if ($errors->has('name'))
            <span class="help-block">
                                        <strong>{{ $errors->first('serial_number') }}</strong>
                                    </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Name</label>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) !!}
        </div>
        @if ($errors->has('name'))
            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('qty_per_carton') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Qty/Carton</label>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::text('qty_per_carton', null, ['class' => 'form-control', 'id' => 'qty_per_carton']) !!}
        </div>

        @if ($errors->has('qty_per_carton'))
            <span class="help-block">
                                        <strong>{{ $errors->first('qty_per_carton') }}</strong>
                                    </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('unit_price') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Unit Price</label>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::text('unit_price', null, ['class' => 'form-control', 'id' => 'unit_price']) !!}
        </div>
        @if ($errors->has('unit_price'))
            <span class="help-block">
                                        <strong>{{ $errors->first('unit_price') }}</strong>
                                    </span>
        @endif
    </div>
</div>
<div class="form-group{{ $errors->has('cartons') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label"># of Cartons</label>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::text('cartons', null, ['class' => 'form-control', 'id' => 'cartons']) !!}
        </div>

        @if ($errors->has('cartons'))
            <span class="help-block">
                                        <strong>{{ $errors->first('cartons') }}</strong>
                                    </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Upload Photo</label>

    <div class="col-md-6">
        <div class="form-group">
            <input type="file" class="form-control" name="photo">
        </div>

        @if ($errors->has('photo'))
            <span class="help-block">
                <strong>{{ $errors->first('photo') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
            Save
        </button>
    </div>
</div>
