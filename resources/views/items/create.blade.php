@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Add Item</div>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if(Session::has('message'))

        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Info!</strong>  {{Session::get('message')}}
        </div>
    @endif
                    <div class="panel-body">
                        {!!Form::open(array('route' => ['items.store']  , 'method' => 'POST', 'files' => 'true'))!!}

                            @include('items._form')

                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection