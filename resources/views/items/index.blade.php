@extends('layouts.app')

@section('content')
    @if(Session::has('message'))

        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Info!</strong>  {{Session::get('message')}}
        </div>
    @endif

    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="col-md-6">
                            <h5>Manage Items
                            </h5>
                        </div>
                        <div class="col-md-6 pull-right">
                            <a style="margin-left: 5px;" href="{!! route('items.create') !!}" class="btn btn-primary btn-sm pull-right"> <i class="fa fa-plus"></i> Add Item </a>

                            <form method="get" action="/items" class="pull-right mail-search">
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm" name="search" id="search" value="" placeholder="Search" required>
                                    {{--<input type="hidden" name="email_type" id="email_type">--}}
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-sm btn-primary">
                                            Search
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <br>
                    <div class="ibox-content">

                        <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="8">
                            <thead>
                            <tr>


                                <th>Photo</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Godown</th>
                                <th>Qty/Carton</th>
                                <th># of Carton(s) Left</th>
                                <th>Total Qty Left</th>
                                <th>Unit Price</th>
                                <th>Total Amount</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $total = 0 ?>
                            @forelse($items as $item)
                                <tr>
                                    <td><img src="{{$item->photo}}" alt="item" style="width:130px;height:100px;"></td>
                                    <td>{{$item->serial_number}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{ $item->godown->name }}</td>
                                    <td>{{$item->qty_per_carton}}</td>
                                    <td>{{ number_format($item->cartons, 2)}}</td>
                                    <td>{{ $item->total_qty }}</td>
                                    <td>{{$item->unit_price}}</td>
                                    <td>{{number_format($item->total_amount, 2)}}</td>
                                    <td>
                                        <a href="/items/{{$item->id}}/edit" class="btn btn-success btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                        {{Form::open(array('action' => array('ItemController@destroy', $item->id) , 'method'=>'DELETE', 'style' => 'display:inline'))}}
                                        <button  type="submit" class="btn btn-danger btn-sm">Delete</button>
                                        {{Form::close()}}
                                    </td>

                                </tr>
                                    <?php $total += $item->total_price ?>

                            @empty
                                <tr>
                                    <td colspan="10">No purchase items found!</td>
                                </tr>

                                @endforelse

                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="5">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                        <div class="pull-right"><b>Total Amount:</b> {!! $total !!}.00</div>
                        {{ $items->links() }}

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
