
<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Godown Name</label>

    <div class="col-md-6">
        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
            {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) !!}
        </div>
        @if ($errors->has('name'))
            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Description</label>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description']) !!}
        </div>

        @if ($errors->has('description'))
            <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Status</label>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::select('status', [1 => 'Active', 0 => 'Non-Active'], null, ['class' => 'form-control', 'id' => 'status']) !!}
        </div>

        @if ($errors->has('status'))
            <span class="help-block">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
        @endif
    </div>
</div>

<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
            Save
        </button>
    </div>
</div>