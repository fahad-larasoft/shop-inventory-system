@extends('layouts.app')

@section('content')
    @if(Session::has('message'))

        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Info!</strong>  {{Session::get('message')}}
        </div>
    @endif

    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Manage Godowns
                        </h5>

                        <div class="pull-right"><a href="/godown/create" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add</a></div>
                        <br>
                        <br>
                    </div>
                    <br>
                    <div class="ibox-content">

                        <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="8">
                            <thead>
                            <tr>


                                <th>Name</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($godowns as $godown)
                            <tr>
                                <td>{{$godown->name}}</td>
                                <td>{{date('Y-M-d',strtotime($godown->created_at))}}</td>
                                <td>
                                    @if($godown->status == '1')
                                        <button type="button" class="btn btn-success btn-xs m-l-sm">Active</button>
                                        @else
                                        <button type="button" class="btn btn-danger btn-xs m-l-sm">Non-active</button>
                                        @endif
                                </td>
                                <td>
                                    <a href="/godown/{{$godown->id}}/edit" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                    {{Form::open(array('action' => array('GodownController@destroy', $godown->id) , 'method'=>'DELETE', 'style' => 'display:inline'))}}
                                    <button  type="submit" class="btn btn-danger btn-sm">Delete</button>
                                    {{Form::close()}}
                                </td>

                            </tr>
                            @empty
                                <tr>
                                    <td>No godowns found!</td>
                                </tr>
                            @endforelse
                            </tbody>
                            {{--<tfoot>--}}
                            {{--<tr>--}}
                                {{--<td colspan="5">--}}
                                    {{--<ul class="pagination pull-right"></ul>--}}
                                {{--</td>--}}
                            {{--</tr>--}}
                            {{--</tfoot>--}}
                        </table>
                        {{ $godowns->links() }}

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
