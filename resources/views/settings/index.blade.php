@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row">

            <div class="col-md-8 col-md-offset-1">
                @if(Session::has('message'))

                    <div class="alert alert-info" style="margin-top: 32px;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Info!</strong>  {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Update Settings</div>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="panel-body">
                        {!!Form::open(array('route' => ['settings.update']  , 'method' => 'POST', 'files' => 'true'))!!}

                        <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
                            <label class="col-md-2 col-md-offset-2 control-label">User Name</label>

                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('user_name') ? ' has-error' : '' }}">
                                    {!! Form::text('user_name', auth()->user()->name, ['class' => 'form-control', 'id' => 'user_name', 'required' => 'required']) !!}
                                </div>
                                @if ($errors->has('user_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('shop_name') ? ' has-error' : '' }}">
                            <label class="col-md-2 col-md-offset-2 control-label">Shop Name</label>

                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('shop_name') ? ' has-error' : '' }}">
                                    {!! Form::text('shop_name', auth()->user()->shop->name, ['class' => 'form-control', 'id' => 'shop_name', 'required' => 'required']) !!}
                                </div>
                                @if ($errors->has('shop_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('shop_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('shop_address') ? ' has-error' : '' }}">
                            <label class="col-md-2 col-md-offset-2 control-label">Shop Address</label>

                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('shop_address') ? ' has-error' : '' }}">
                                    {!! Form::text('shop_address', auth()->user()->shop->address, ['class' => 'form-control', 'id' => 'shop_address', 'required' => 'required']) !!}
                                </div>
                                @if ($errors->has('shop_address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('shop_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('shop_phone') ? ' has-error' : '' }}">
                            <label class="col-md-2 col-md-offset-2 control-label">Shop Phone</label>

                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('shop_phone') ? ' has-error' : '' }}">
                                    {!! Form::text('shop_phone', auth()->user()->shop->phone, ['class' => 'form-control', 'id' => 'shop_phone', 'required' => 'required']) !!}
                                </div>
                                @if ($errors->has('shop_phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('shop_phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                                <a href="" class="btn btn-default">Cancel</a>
                            </div>
                        </div>

                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>





@endsection
