<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="/images/e-commerce.png"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Shop Inventory System</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Scripts -->
{{--    <script src="{{ asset('js/theme.js') }}" defer></script>--}}

<!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/theme.css') }}" rel="stylesheet">
    @yield('styles')
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
        .panel-default {
            margin-top: 32px !important;
        }
    </style>
</head>
<body>
<div id="wrapper">

    @if(auth()->check())
        @include('partials._left_sidebar')
    @endif

    @if(auth()->check())
        <div id="page-wrapper" class="gray-bg dashbard-1">
            @include('partials._header')
            @endif

            @yield('content')

            @if(auth()->check())
        </div>
    @endif


    @if(auth()->check())
        {{--@include('partials._small_chat')--}}

        @include('partials._right_sidebar')
    @endif
</div>

<script src="{{asset('/js/theme.js')}}"></script>

@yield('scripts')
</body>
</html>

