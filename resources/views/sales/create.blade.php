@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Add Sale Items </div>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="panel-body">
                        {!!Form::open(array('route' => ['sales.store']  , 'method' => 'POST', 'files' => 'true'))!!}

                            @include('sales._form')

                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(".search_select").select2();

        $('#remove').click(function(){
            var el = $('#count');
            var num = parseInt(el.text());
            $('#drug'+num).remove();
            $('#qty'+num).remove();
            $('#unit_price'+num).remove();
            $('.data').find('br:last-child').remove();
            el.text(num-1);
            $('#number').val(num-1);
        });

        $('#add').click(function() {
            console.log($('select:last').val());
            if ($('select:last').val() == null) {
                $('select:last').next('.error').text('Please select this item first to add more.');
            } else {
                $('select:last').next('.error').text('');
                var el = $('#count');
                var num = parseInt(el.text()) + 1;
                el.text(num);
                $('#number').val(num);

                var selected_item_ids = [];
                $.each($('select'), function () {
                    if ($(this).val() != '') {
                        selected_item_ids.push($(this).val())
                    }
                });

                $.ajax({
                    url: "{{ url('sales/items/html') }}",
                    data: {num: num, selected_item_ids: selected_item_ids},
                    success: function (data) {
                        $('.data').append(data);
                    }
                });

            }
        });

        $(document).on( 'change', 'select', function(){
            var drug_id = $(this).val();
            var element_id = $(this).attr('id');

            $.ajax({
                url: "{{url('sales/items/validate/qty')}}",
                data: {item_id: drug_id},
                success: function(data) {
                    $('[name="qty'+element_id+'"]').attr('max', data);
                }});
        });


    </script>

@stop
