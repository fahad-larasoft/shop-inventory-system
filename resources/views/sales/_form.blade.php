<br/>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="form-group">
                <label for="cust_name"> Customer Name </label>&nbsp
                <input type="text" style="width: 300px" class="form-control" name="customer_name" required="true"/>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="form-group">
                <label for="cust_name"> Customer City </label>&nbsp
                <input type="text" style="width: 300px" class="form-control" name="customer_city" required="true"/>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="form-group">
                <label for="cust_name"> Customer Phone </label>&nbsp
                <input type="text" style="width: 300px" class="form-control" name="customer_phone" required="true"/>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="form-group">
                <label for="comments">Comments:</label>
                {!! Form::textarea('comments', null, ['class' => 'form-control', 'id' => 'comments', 'style' => 'width: 300px']) !!}
            </div>

        </div>
    </div>
</div>

<br><br>
<div class="row">
    <div class="col-md-2 col-md-offset-10">
        <a id="add" class="btn btn-info"><i class="fa fa-plus"></i></a>
        <a id="remove" class="btn btn-info"><i class="fa fa-minus"></i></a>
    </div>
</div>
</br>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-4" style="margin-bottom: 30px">
        <div class="form-group">
            <div class="form-group">
                <label> Select Product </label>&nbsp
                <select required style="width: 300px" name="id1" id="1" class="form-control search_select">
                    <option value="" disabled selected>Select a Product</option>
                    @foreach($items as $item)
                        <option value="{{$item->id}}">{{ $item->title }}</option>
                    @endforeach
                </select>
                <p class="error" style="color: red;"></p>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label for="qty">Price</label> &nbsp
            <input style="width: 200px" type="number" required min="0" name="unit_price" class="form-control" id="unit_price">
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">
            <label for="qty">Quantity</label> &nbsp
            <input style="width: 200px" type="number" required min="0" name="qty1" class="form-control" id="qty">
        </div>
    </div>
    

    <div class="clearfix"></div>
    <div id="count" style="display: none">1</div>
    <input type="text" id="number" hidden="true" value="1" name="number"/>
    <div class="data"></div>
    <br/><br/><br/>

    <div class="col-md-1" style="margin-bottom: 30px">
        <button type="submit" class="btn btn-block btn-primary">Save</button>
    </div>
    <div class="col-md-1" style="margin-bottom: 30px">
        <a href="{{ \Illuminate\Support\Facades\URL::previous() }}" class="btn btn-block btn-default"> Back</a>
    </div>
</div>


    <br/><br/><br/>

