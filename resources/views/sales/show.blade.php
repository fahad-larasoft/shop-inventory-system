@extends('layouts.app')
@section('styles')

    <style>

        th, td {
            text-align: center;
              }

    </style>

@stop
@section('content')


<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Invoice</h2>

    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a href="{!! URL::previous() !!}" style="margin-right: 5px" class="btn btn-default"> <i class="fa fa-backward"></i> Back</a>
            <button class="btn btn-primary print-btn"><i class="fa fa-print"></i> Print Invoice </button>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox-content p-xl print-invoice">
            <style type="text/css">

              table{
                border: 1px solid  #800000;
                width: 100%;
              }
              th, td {
                 border: 1px solid #e7eaec;
              }

                table, tr, td {
                    border-collapse: collapse;
                }

              tr:nth-child(even){
                background-color: #f2f2f2
            }


            </style>
                <div class="row">
                    <div style="text-align: center;" class="col-md-12">
                        <img src="{!! config('logo.logo-image') !!}" style="height: 150px; width: 150px;" alt="">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4" style="display:inline-block; width: 300px;">
                        <h5>From:</h5>
                        <address>
                            <strong>{!! auth()->user()->shop->name !!}</strong><br>
                            {!! auth()->user()->shop->address !!}<br>
                            {{--Chicago, VT 32456<br>--}}
                            <abbr title="Phone">Phone:</abbr> {!! auth()->user()->shop->phone !!}
                        </address>
                    </div>

                    <div style="display:inline-block; width: 27%"></div>

                    <div class="col-md-4 text-right pull-right" style="display:inline-block; text-align: right">
                        <h4>Invoice No.</h4>
                        <h4 class="text-navy">INV-{!! $invoice->id !!}-00</h4>
                        <span>To:</span>
                        <address>
                            <strong>{!! $invoice->customer_name !!}</strong><br>
                            {!! $invoice->customer_city !!}<br>
                            <abbr title="Phone">Phone:</abbr> {!! $invoice->customer_phone !!}
                        </address>
                        <p>
                            <span><strong>Invoice Date:</strong> {!! $invoice->created_at->format('d M, Y') !!}</span><br/>
                            {{--<span><strong>Due Date:</strong> March 24, 2014</span>--}}
                        </p>
                    </div>
                </div>

                <div class="table-responsive m-t">
                    <table class="table invoice-table table-bordered">
                        <thead>
                            <tr>
                                <th>Photo</th>
                                <th>Code</th>
                                <th>Item Name</th>
                                <th>Unit Price</th>
                                <th>Packing/PCS</th>
                                <th>Cartons</th>
                                <th>Total Price</th>
                                <th class="no-print">+/-</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($invoice->itemsSold as $item)
                            <tr>
                                <td><img src="{{url($item->photo)}}" alt="item" style="width:90px;height:70px;"></td>
                                <td>
                                    {!! $item->serial_number !!}
                                </td>
                                <td>{!! $item->name !!}</td>
                                <td>{!! number_format($item->unit_price, 2) !!}</td>
                                <td>{!! $item->qty !!}</td>
                                <td>{!! isset($item->cartons) ? number_format($item->cartons, 2) : 'N/A' !!}</td>
                                <td>{!! $item->total_price !!}</td>
                                <td class="no-print">
                                @if($item->unit_price > $item->original_price)
                                +{!! $item->unit_price - $item->original_price !!}
                                @elseif($item->unit_price < $item->original_price)
                                 {!! $item->unit_price - $item->original_price !!}
                                 @endif
                                </td>
                            </tr>
                                @endforeach
                        </tbody>
                    </table>
                </div><!-- /table-responsive -->

                <table class="table invoice-total">
                    <tbody>
                        {{--<tr>--}}
                            {{--<td><strong>Sub Total :</strong></td>--}}
                            {{--<td>$1026.00</td>--}}
                        {{--</tr>--}}
                        {{--<tr>--}}
                            {{--<td><strong>TAX :</strong></td>--}}
                            {{--<td>$235.98</td>--}}
                        {{--</tr>--}}
                        <tr>
                            <td style="text-align: right"><strong>TOTAL :</strong> {!! number_format($invoice->total_payable, 2) !!}</td>
                        </tr>
                    </tbody>
                </table>
                {{--<div class="text-right">--}}
                    {{--<button class="btn btn-primary"><i class="fa fa-dollar"></i> Make A Payment</button>--}}
                {{--</div>--}}

                <div class="well m-t"><strong>Comments:</strong>
                    {!! $invoice->comments !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
    <script type="text/javascript">

        $('.print-btn').click(function(){
       
            $(".print-invoice").print({
                globalStyles: false,
                mediaPrint: true,
                stylesheet: null,
                noPrintSelector: ".no-print",
                iframe: false,
                append: null,
                prepend: null,
                manuallyCopyFormValues: true,
                deferred: $.Deferred(),
                timeout: 300,
                title: null,
                doctype: '<!doctype html>'
            });

        });

        $( "tr:odd" ).css( "background-color", "#d9edf7" );
        $( "tr:last" ).css( "background-color", "#fcf8e3" );

    </script>
@stop