@extends('layouts.app')

@section('content')
    @if(Session::has('message'))

        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Info!</strong>  {{Session::get('message')}}
        </div>
    @endif

    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Sale Invoices
                        </h5>

                        <div class="col-md-6 pull-right">
                            <a style="margin-left: 5px;" href="{!! route('sales.create') !!}" class="btn btn-primary btn-sm pull-right"> <i class="fa fa-plus"></i> Add Sale </a>

                            <form method="get" action="" class="pull-right mail-search">
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm" name="search" id="search" value="" placeholder="Search" required>
                                    {{--<input type="hidden" name="email_type" id="email_type">--}}
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-sm btn-primary">
                                            Search
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <br>
                    <div class="ibox-content">

                        <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="8">
                            <thead>
                            <tr>
                                <th>Invoice #</th>
                                <th>Sale Date</th>
                                <th>Customer Name</th>
                                <th>Customer City</th>
                                <th>Customer Phone</th>
                                <th>Total Bill</th>
                                <th>Items</th>

                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php $total = 0 ?>
                                @forelse($invoices as $invoice)
                                    <tr>
                                        <td>
                                            INV-{!! $invoice->id !!}-00
                                        </td>
                                        <td>
                                            {!! $invoice->created_at->format('d M, Y') !!}
                                        </td>
                                        <td>{!! $invoice->customer_name !!}</td>
                                        <td>{!! $invoice->customer_city !!}</td>
                                        <td>{!! $invoice->customer_phone !!}</td>
                                        <td>{!! $invoice->total_payable !!}</td>
                                        <td>{!! $invoice->itemsSold->count() !!}</td>

                                        <td>
                                            <a href="{!! route('sales.show', $invoice->id) !!}" class="btn btn-primary btn-sm"> View Invoice </a>

                                            {{Form::open(array('action' => array('SalesController@destroy', $invoice->id) , 'method'=>'DELETE', 'style' => 'display:inline'))}}
                                            <button  type="submit" class="btn btn-danger btn-sm">Delete</button>
                                            {{Form::close()}}
                                        </td>
                                    </tr>
                                        <?php $total += $invoice->total_payable ?>

                                @empty
                                    <tr>
                                        <td colspan="8">No sales items found!</td>
                                    </tr>

                                @endforelse
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="5">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                        <div class="pull-right"><b>Total Amount:</b> {!! $total !!}.00</div>
                        {{ $invoices->links() }}

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
