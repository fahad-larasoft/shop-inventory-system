const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

mix.styles([
    'resources/assets/theme/css/bootstrap.min.css',
    'resources/assets/theme/font-awesome/css/font-awesome.css',
    'resources/assets/theme/css/plugins/toastr/toastr.min.css',
    'resources/assets/theme/js/plugins/gritter/jquery.gritter.css',
    'resources/assets/theme/css/animate.css',
    'resources/assets/theme/css/style.css',
    'resources/assets/css/dataTables.bootstrap.min.css',
    'resources/assets/css/select2.css',
], 'public/css/theme.css');

mix.scripts([
    'resources/assets/theme/js/jquery-2.1.1.js',
    'resources/assets/theme/js/bootstrap.min.js',
    'resources/assets/theme/js/plugins/metisMenu/jquery.metisMenu.js',
    'resources/assets/theme/js/plugins/slimscroll/jquery.slimscroll.min.js',
    'resources/assets/theme/js/plugins/flot/jquery.flot.js',
    'resources/assets/theme/js/plugins/flot/jquery.flot.tooltip.min.js',
    'resources/assets/theme/js/plugins/flot/jquery.flot.spline.js',
    'resources/assets/theme/js/plugins/flot/jquery.flot.resize.js',
    'resources/assets/theme/js/plugins/flot/jquery.flot.pie.js',
    'resources/assets/theme/js/plugins/peity/jquery.peity.min.js',
    'resources/assets/theme/js/demo/peity-demo.js',
    'resources/assets/theme/js/inspinia.js',
    'resources/assets/theme/js/plugins/pace/pace.min.js',
    'resources/assets/theme/js/plugins/jquery-ui/jquery-ui.min.js',
    'resources/assets/theme/js/plugins/gritter/jquery.gritter.min.js',
    'resources/assets/theme/js/plugins/sparkline/jquery.sparkline.min.js',
    'resources/assets/theme/js/demo/sparkline-demo.js',
    'resources/assets/theme/js/plugins/chartJs/Chart.min.js',
    'resources/assets/theme/js/plugins/toastr/toastr.min.js',
    'resources/assets/js/jquery.dataTables.min.js',
    'resources/assets/js/dataTables.bootstrap.min.js',
    'resources/assets/js/jQuery.print.js',
    'resources/assets/js/select2.js'
], 'public/js/theme.js');
