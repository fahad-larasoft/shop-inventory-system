<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id')->unsigned();
            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('cascade');
            $table->integer('godown_id')->unsigned()->nullable();
            $table->foreign('godown_id')->references('id')->on('godowns')->onDelete('cascade');

            $table->string('serial_number')->unique();
            $table->string('name');
            $table->string('qty_per_carton');
            $table->double('cartons')->nullable();
            $table->integer('total_qty');
            $table->decimal('unit_price');
            $table->decimal('total_price');
            $table->string('photo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items');
    }
}
