<?php

namespace App\Tenant;

use \Illuminate\Database\Eloquent\Scope;
use \Illuminate\Database\Eloquent\Builder;
use \Illuminate\Database\Eloquent\Model;

class TenantShopScope implements Scope{

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        if ( auth()->check() )
        {
            $tenant_column = 'shop_id';
            $builder->where($tenant_column, auth()->user()->shop_id);
        }
    }

    public function remove(Builder $builder)
    {
        $query = $builder->getQuery();

        foreach ( (array) $query->wheres as $key => $where )
        {
            if ( $where['column'] == 'shop_id' )
            {
                unset( $query->wheres[$key] );

                $query->wheres = array_values($query->wheres);
            }
        }
    }
}
