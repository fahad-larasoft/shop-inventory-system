<?php
/**
 * Created by PhpStorm.
 * User: fahad
 * Date: 05/05/2016
 * Time: 6:53 AM
 */

namespace App\Tenant;


trait TenantShopTrait {

    public static function bootTenantShopTrait () {
        static::addGlobalScope(new TenantShopScope());
    }
}
