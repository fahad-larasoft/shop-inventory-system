<?php

namespace App;

use App\Tenant\TenantShopTrait;
use Illuminate\Database\Eloquent\Model;

class ItemSold extends Model
{
    use TenantShopTrait;
    
    protected $table = 'items_sold';

    protected $fillable = [
        'shop_id',
        'item_id',
        'invoice_id',
        'serial_number',
        'name',
        'qty',
        'unit_price',
        'total_price',
        'photo',
        'original_price',
        'cartons'
    ];

    // item_id = nullable [Please reference this relation carefully]
    public function item(){
        return $this->belongsTo(Item::class);
    }

    public function invoice(){
        return $this->belongsTo(Invoice::class);
    }
}
