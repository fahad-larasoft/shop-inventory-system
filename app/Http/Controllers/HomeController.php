<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function profile()
    {
        return view('profile');
    }

    public function updateProfile(Request $request)
    {
        auth()->user()->update(['name' => $request->user_name]);
        auth()->user()->shop->update([
            'name' => $request->shop_name,
            'address' => $request->shop_address,
            'phone' => $request->shop_phone,
        ]);

        return redirect()->back()->with('message' , "Profile Updated successfully!");
    }
}
