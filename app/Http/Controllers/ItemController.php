<?php

namespace App\Http\Controllers;

use App\Godown;
use App\Http\Requests;
use App\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class ItemController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        if ( $search )
        {
            $items = Item::where('serial_number', 'like', "%$search%")
                ->orWhere('name', 'like', "%$search%")
                ->orWhere('qty_per_carton', 'like', "%$search%")
                ->orWhere('unit_price', 'like', "%$search%")
                ->orWhere('cartons', 'like', "%$search%")
                ->orWhere('total_price', 'like', "%$search%")
                ->orderBy('created_at', 'DESC')
                ->paginate(50);
            $items->setPath('?search='.$search);
            return view('items.index', compact('items'));
        }
        else
        {
            $items = Item::paginate(50);

            return view('items.index', compact('items'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $godowns = Godown::where('status', 1)->pluck('name', 'id');

        return view('items.create', compact('godowns'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = array(
            'serial_number'  => 'required|max:255|unique:items',
            'name'           => 'required',
            'qty_per_carton' => 'required|integer',
            'unit_price'     => 'required|numeric',
            'cartons'        => 'required|numeric',
            'godown_id'      => 'required'
        );
        $item = new Item();
        $validator = Validator::make($request->all(), $rules);
        if ( $validator->fails() )
        {
            return back()->with(['errors' => $validator->messages()])->withInput($request->all());
        }

        if ( $request->hasFile('photo') )
        {
            $fileName = Self::imageUpload($item, $request);

        }
        else
        {
            return redirect()->back()->with('message', 'Image is required');
        }

        if ( $request->cartons == 0 || $request->cartons == '')
        {
            $amount = $request->qty_per_carton * $request->unit_price;
            $total_qty = $request->qty_per_carton;
            $item->cartons = 0;
        }
        else
        {
            $amount = $request->qty_per_carton * $request->unit_price * $request->cartons;
            $total_qty = $request->qty_per_carton * $request->cartons;
            $item->cartons = $request->cartons;
        }

        $item->shop_id = Auth::user()->shop_id;
        $item->godown_id = $request->godown_id;
        $item->serial_number = $request->serial_number;
        $item->name = $request->name;
        $item->qty_per_carton = $request->qty_per_carton;
        $item->unit_price = $request->unit_price;
        $item->photo = $fileName;
        $item->total_price = $amount;
        $item->total_qty = $total_qty;
        $item->save();

        return redirect('items')->with('message', "Item has been created!");
    }

    public static function imageUpload($item, $request)
    {
        if ( $item->image != null )
        {
            if ( File::exists(public_path() . $item->photo) )
            {
                unlink(public_path() . $item->photo);
            }
        }

        $file = $request->file('photo');
        $destinationPath = public_path() . '/uploads/items';
        if ( !File::exists($destinationPath) )
        {
            mkdir($destinationPath, 0777, true);
        }
        $fileName = $file->getClientOriginalName();
        $fileName = time() . "_" . $fileName;
        $fileName = str_replace(" ", "_", $fileName);
        $file->move($destinationPath, $fileName);


        return '/uploads/items/' . $fileName;

    }






    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::find($id);
        $godowns = Godown::where('status', 1)->pluck('name', 'id');

        return view('items.edit', compact('item', 'godowns'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'serial_number'  => 'required',
            'name'           => 'required',
            'qty_per_carton' => 'required|integer',
            'unit_price'     => 'required|numeric',
            'cartons'        => 'integer'

        );

        $item = Item::find($id);
        $validator = Validator::make($request->all(), $rules);
        if ( $validator->fails() )
        {
            return back()->with(['errors' => $validator->messages()])->withInput($request->all());
        }
        if ( $request->cartons == 0 || $request->cartons == '')
        {
            $amount = $request->qty_per_carton * $request->unit_price;
            $total_qty = $request->qty_per_carton;
            $item->cartons = 0;
        }
        else
        {
            $amount = $request->qty_per_carton * $request->unit_price * $request->cartons;
            $total_qty = $request->qty_per_carton * $request->cartons;
            $item->cartons = $request->cartons;

        }

        if ( $request->hasFile('photo') )
        {
            $fileName = $this->imageUpload($item, $request);

            $item->photo = $fileName;
        }

        $item->shop_id = Auth::user()->shop_id;
        $item->godown_id = $request->godown_id;
        $item->serial_number = $request->serial_number;
        $item->name = $request->name;
        $item->qty_per_carton = $request->qty_per_carton;
        $item->unit_price = $request->unit_price;
        $item->total_price = $amount;
        $item->total_qty = $total_qty;
        $item->save();

        return redirect('items')->with('message', "Item has been Updated!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::find($id);
        if ( File::exists(public_path() . $item->photo) )
        {
            unlink(public_path() . $item->photo);
        }
        $item->delete();

        return redirect()->route('items.index')->with('message', 'Deleted Successfully!');
    }
}
