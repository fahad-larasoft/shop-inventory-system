<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('settings.index');
    }

    public function update(Request $request)
    {
        auth()->user()->update(['name' => $request->user_name]);
        auth()->user()->shop->update([
            'name' => $request->shop_name,
            'address' => $request->shop_address,
            'phone' => $request->shop_phone,
        ]);

        return redirect()->back()->with('message' , "Profile Updated successfully!");
    }
}
