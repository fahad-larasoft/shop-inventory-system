<?php

namespace App\Http\Controllers;

use App\Godown;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class GodownController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $godowns = Godown::paginate(10);
        return view('godown.index',compact('godowns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('godown.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'status' => 'required'

        );

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
            return back()->with(['errors' => $validator->messages()])->withInput($request->all());
        }

        $godown = new Godown();
        $godown->name = $request->name;
        $godown->description = $request->description;
        $godown->status = $request->status;
        $godown->shop_id = Auth::user()->shop_id;
        $godown->save();

        return redirect('godown')->with('message' , "Godown has been created!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $godown = Godown::find($id);
        return view('godown.edit',compact('godown'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name' => 'required',
            'status' => 'required'

        );

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
            return back()->with(['errors' => $validator->messages()])->withInput($request->all());
        }

        $godown = Godown::find($id);
        $godown->name = $request->name;
        $godown->description = $request->description;
        $godown->status = $request->status;
        $godown->shop_id =Auth::user()->shop_id;
        $godown->save();



        return redirect('godown')->with('message' , "Godown has been Updated!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $godown = Godown::find($id);
        $godown->delete();

        return redirect()->route('godown.index')->with('message' , 'Deleted Successfully!');
    }
}
