<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Item;
use App\ItemSold;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = request()->get('search');
        if ( $search )
        {
            $invoices = Invoice::where('customer_name', 'like', "%$search%")
                ->orWhere('customer_city', 'like', "%$search%")
                ->orWhere('customer_phone', 'like', "%$search%")
                ->orWhere('total_payable', 'like', "%$search%")
                ->orWhere('invoice_number', 'like', "%$search%")
                ->orWhere('created_at', 'like', "%$search%")
                ->orderBy('created_at', 'DESC')
                ->paginate(50);
            $invoices->setPath('?search='.$search);
        }
        else
        {
            $invoices = Invoice::paginate(50);
        }
        return view('sales.index', compact('invoices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = Item::where('shop_id', auth()->user()->shop_id)->get();
        return view('sales.create', compact('items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->get('id1') != null && $request->get('qty1') != null)
        {
            $invoice = Invoice::create([
                'created_by' => Auth::user()->name,
                'customer_name' => $request->get('customer_name'),
                'customer_city' => $request->get('customer_city'),
                'customer_phone' => $request->get('customer_phone'),
                'shop_id' => auth()->user()->shop_id,
                'comments' => $request->comments
            ]);
            for ($i = 1; $i <= $request->get('number'); $i++)
            {
                $item = Item::find($request->get('id' . $i));
                $qty_per_carton = $item->qty_per_carton;

                $itemSale = ItemSold::create([
                    'serial_number' => $item->serial_number,
                    'shop_id' => auth()->user()->shop_id,
                    'item_id' => $item->id,
                    'name' => $item->name,
                    'qty' => $request->get('qty' . $i),
                    'total_price' => $request->get('qty' . $i) * $request->get('unit_price'),
                    'unit_price' => $request->get('unit_price'),
                    'original_price' => $item->unit_price,
                    'invoice_id' => $invoice->id
                ]);

                if($item->cartons > 0) {
                    $itemSale->cartons = ($itemSale->qty / $qty_per_carton) > 0 ? $itemSale->qty / $qty_per_carton : null;
                }

                if ( ( $request->get('qty' . $i) - $item->qty_per_carton ) >= 0 )
                {
                    if($item->cartons > 0){
                        $cartons = ( $item->total_qty - $request->get('qty' . $i) ) / $item->qty_per_carton;
                        $item->cartons = $cartons;

                    }

                    $total_qty = $item->total_qty - $request->get('qty' . $i);
                    $item->total_qty = $total_qty;
                }
                else
                {
                    $total_qty = $item->total_qty - $request->get('qty' . $i);
                    $item->total_qty = $total_qty;

                    if($item->cartons > 0)
                    {
                        $cartons = ($item->total_qty / $item->qty_per_carton);
                        $item->cartons = $cartons;
                    }
                }

                $item->save();

                $photo = $this->saleimageUpload($itemSale);

                $itemSale->photo = '/uploads/items_sale/'.$photo;
                $itemSale->save();
            }
            $invoice->total_payable = $invoice->itemsSold->sum('total_price');
            $invoice->invoice_number = "INV-{$invoice->id}-00";
            $invoice->save();
        }

        return redirect()->route('sales.index')->with('message', 'Sale has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoice = Invoice::find($id);

        return view('sales.show', compact('invoice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public static function saleimageUpload($itemSold)
    {
//        dd($item->photo);
        $file = public_path($itemSold->item->photo);

        $destinationPath = public_path('/uploads/items_sale');

        $newFile = public_path('/uploads/items_sale/'.explode('/', $itemSold->item->photo)[3]);

        if ( !File::exists($destinationPath) )
        {
            mkdir($destinationPath, 0777, true);
        }

        copy($file, $newFile);

        return explode('/', $itemSold->item->photo)[3];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

         $invoices =Invoice::find($id);
        foreach($invoices->itemsSold as $sale_item){

            if ( File::exists(url($sale_item->photo)) )
            {
                unlink(url($sale_item->photo));
            }

        }

        $invoices->delete();
        return redirect()->back()->with('message', 'Sale has been deleted successfully!');
    }

    public function getHtml(){

        $items = Item::whereNotIn('id', request('selected_item_ids'))->get();
        $html = "<option value='' disabled selected>Select a Product</option>";
        foreach($items as $item){
            $html .= "<option value='$item->id'>$item->title</option>";
        }

        $num = request('num');
        return '<div style="margin-bottom: 30px" class="col-md-4" id="drug'.$num.'">'.
                    '<div class="form-group">'.
                        '<div class="form-group">'.
                            '<label> Select Product </label> &nbsp'.
                            '<select required style="width: 300px" id="'.$num.'" name="id'.$num.'" class="form-control">'.
                                $html.
                            '</select>'.
                            '<p class="error" style="color: red;"></p>'.
                        '</div>'.
                    '</div>'.
                '</div>'.
                '<div style="margin-bottom: 30px" class="col-md-4" id="unit_price'.$num.'">'.
                    '<div class="form-group">'.
                        '<label for="qty">Price </label> &nbsp'.
                        '<input required="true" type="number" style="width: 200px" min="0" name="unit_price'.$num.'" class="form-control" id="unit_price">'.
                    '</div>'.
                '</div>' .
                '<div style="margin-bottom: 30px" class="col-md-4" id="qty'.$num.'">'.
                    '<div class="form-group">'.
                        '<label for="qty">Quantity </label> &nbsp'.
                        '<input required="true" type="number" style="width: 200px" min="0" name="qty'.$num.'" class="form-control" id="qty">'.
                    '</div>'.
                '</div>' .

                '<div class="clearfix"></div>';
    }

    public function validateQty(){
        $item =Item::find(request('item_id'));
        return $item->total_qty;
    }

}
