<?php

namespace App;

use App\Tenant\TenantShopTrait;
use Illuminate\Database\Eloquent\Model;

class Godown extends Model
{
    use TenantShopTrait;
}
