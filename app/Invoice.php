<?php

namespace App;

use App\Tenant\TenantShopTrait;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use TenantShopTrait;

    protected $fillable = [
        'customer_name',
        'customer_city',
        'customer_phone',
        'total_payable',
        'discount',
        'is_paid',
        'created_by',
        'shop_id',
        'comments',
        'invoice_number'
    ];

    public function shop(){
        return $this->belongsTo(Shop::class);
    }

    public function itemsSold(){
        return $this->hasMany(ItemSold::class);
    }
}
