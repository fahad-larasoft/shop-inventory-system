<?php

namespace App;

use App\Tenant\TenantShopTrait;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use TenantShopTrait;

    public function godown(){
        return $this->belongsTo(Godown::class);
    }

    public function getTotalAmountAttribute(){

        if($this->cartons == 0){
            return $this->qty_per_carton * $this->unit_price;
        }else{
            return $this->qty_per_carton * $this->unit_price * $this->cartons;
        }
    }

    public function getTitleAttribute()
    {
        return $this->serial_number . ' - ' . $this->name;
    }
}
