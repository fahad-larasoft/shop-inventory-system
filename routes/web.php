<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function(){

    Route::get('/', 'HomeController@index');
    Route::get('/settings', 'SettingsController@index')->name('settings.index');
    Route::post('/settings', 'SettingsController@update')->name('settings.update');

    Route::resource('godown', 'GodownController');
    Route::resource('items', 'ItemController');
    Route::get('sales/items/html', 'SalesController@getHtml');
    Route::get('sales/items/validate/qty', 'SalesController@validateQty');

    Route::resource('sales', 'SalesController');
});
